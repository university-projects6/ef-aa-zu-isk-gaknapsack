package model;

import com.softtechdesign.ga.*;
import enums.KnapsackTypeEnum;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * Solving knapsack problem using genetic algorithm
 */
public class GAKnapsack extends GAString {

    /**
     * Knapsack problem type
     */
    private static final KnapsackTypeEnum KNAPSACK_TYPE = KnapsackTypeEnum.LINEAR;
    /**
     * Number of decimal places possible in gene value (only for LINEAR type)
     */
    private static final int DECIMAL_PLACES = 2;

    /**
     * Size of the backpack
     */
    private static double backpackSize;
    /**
     * Items available to pick
     */
    private static HashMap<Integer, Item> availableItems;

    /**
     * Initializes parameters of genetic algorithm from super class
     * @throws GAException when initialization fails
     */
    public GAKnapsack(
            String inputFilename,
            int populationSize,
            double crossoverProbability,
            int randomSelectionChance,
            int generations,
            double mutationProbability
    ) throws GAException, FileNotFoundException {
        super(
            initializeParameters(inputFilename), // number of chars in chromosome
            populationSize, // number of chromosomes (population size)
            crossoverProbability, // crossover probability
            randomSelectionChance, // random selection chance (regardless of fitness)
            generations, // number of generations
            0, // number of preliminary runs
            0, // maximum number of generations in preliminary runs
            mutationProbability, // chromosome mutation probability
            0, // number of decimal places in chromosome (only when treated as double)
            KNAPSACK_TYPE.getValuesRange(), // possible gene values
            Crossover.ctTwoPoint, // crossover type
            true // compute statistics
        );
    }

    /**
     * Calculate how well chromosome reflects best solution
     * @param chromIdx index of chromosome
     * @return fitness of a chromosome to correct solution
     */
    @Override
    protected double getFitness(int chromIdx) {
        // prepare genes values
        ChromChars chromosome = this.getChromosome(chromIdx);
        String chromValue = chromosome.getGenesAsStr();
        List<String> genes = new ArrayList<>();
        int geneSize = KNAPSACK_TYPE.equals(KnapsackTypeEnum.BINARY) ? 1 : 1 + DECIMAL_PLACES;
        for (int idx = 0; idx < chromValue.length(); idx += geneSize) {
            genes.add(chromValue.substring(idx, Math.min(idx + geneSize, chromValue.length())));
        }
        if (KNAPSACK_TYPE.equals(KnapsackTypeEnum.LINEAR)) {
            genes = genes.stream().map(gene ->
                gene.substring(0, 1) + '.' + gene.substring(1)
            ).collect(Collectors.toList());
        }

        // calculate fitness
        double fitness = 0;
        double volume = 0;
        switch (KNAPSACK_TYPE) {
            case BINARY:
                for (int idx = 0; idx < genes.size(); idx++) {
                    if (Integer.parseInt(genes.get(idx)) == 1) {
                        volume += availableItems.get(idx + 1).getVolume();
                        fitness += availableItems.get(idx + 1).getPrice();
                    }
                }
                break;
            case LINEAR:
                int earlyAccelerator = genes.size();
                for (int idx = 0; idx < genes.size(); idx++) {
                    double geneValue = Double.parseDouble(genes.get(idx));
                    if (geneValue > 1.0) {
                        earlyAccelerator--;
                    }
                    if (geneValue > 0.0) {
                        volume += availableItems.get(idx + 1).getVolume() * geneValue;
                        fitness += availableItems.get(idx + 1).getPrice() * geneValue;
                    }
                }
                if (earlyAccelerator > 0 && earlyAccelerator < genes.size()) {
                    return earlyAccelerator;
                }
                break;
        }

        // check if solution fits to backpack size and return fitness
        if (volume <= backpackSize) {
            return fitness;
        } else {
            return 0;
        }
    }

    /**
     * Reads file with input data and initializes class fields
     * @param filename path to a file
     * @throws FileNotFoundException when file was not found
     */
    private static void readFile(String filename) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(filename));

        // read size of backpack
        backpackSize = scanner.nextDouble();

        // read available items
        HashMap<Integer, Item> items = new HashMap<>();
        while (scanner.hasNextLine()) {
            int itemIdx = scanner.nextInt();
            double itemVolume = scanner.nextDouble();
            double itemPrice = scanner.nextDouble();

            items.put(itemIdx, new Item(itemVolume, itemPrice));
        }
        availableItems = items;
    }

    private static int initializeParameters(String inputFilename) throws FileNotFoundException {
        // read file with input data
        // and initialize class fields
        String inputPath = "./src/input/";
        readFile(inputPath.concat(inputFilename));

        switch (KNAPSACK_TYPE) {
            case BINARY:
                return availableItems.size();
            case LINEAR:
                return availableItems.size() * (DECIMAL_PLACES + 1);
            default:
                return 0;
        }
    }

}
