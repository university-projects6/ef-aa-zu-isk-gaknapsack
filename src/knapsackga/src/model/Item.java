package model;

/**
 * Backpack item
 */
public class Item {

    /**
     * Volume of the item
     */
    private double volume;
    /**
     * Price of the item
     */
    private double price;

    public Item() {}

    public Item(double volume, double price) {
        this.volume = volume;
        this.price = price;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
