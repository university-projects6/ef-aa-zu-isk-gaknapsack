package services;

import com.softtechdesign.ga.ChromChars;
import com.softtechdesign.ga.GAException;
import model.GAKnapsack;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Runs an experiment to define optimal population size for knapsack problem
 */
public class PopulationSizeRunner {

    /**
     * Starting value of the population size
     */
    private static final int POPULATION_SIZE_MIN = 10;
    /**
     * Ending value of the population size
     */
    private static final int POPULATION_SIZE_MAX = 1000;
    /**
     * Population size loop step
     */
    private static final int POPULATION_SIZE_STEP = 10;
    /**
     * Number of loops done on single population size
     */
    private static final int LOOPS_PER_POPULATION_SIZE = 10;

    /**
     * Crossover probability
     */
    private static final double CROSSOVER_PROBABILITY = 1.0;
    /**
     * Random selection chance
     */
    private static final int RANDOM_SELECTION_CHANCE = 10;
    /**
     * Number of generations
     */
    private static final int GENERATIONS = 1000;
    /**
     * Chromosome mutation probability
     */
    private static final double MUTATION_PROBABILITY = 0.01;

    /**
     * Run the experiment to determine best population size
     * @param inputFilename name of the file with input data
     * @param outputFilename name of the file to write results to
     */
    public void runPopulationSizeLoop(String inputFilename, String outputFilename) {
        try {
            // create writer to store algorithm results in a file
            String outputPath = "./src/output/";
            BufferedWriter writer = new BufferedWriter(
                    new FileWriter(outputPath.concat(outputFilename))
            );

            // write file headers
            writer.write("Population size,Best chromosome,Best fitness,Avg fitness\n");

            // loop through population size
            for (
                    int populationSize = POPULATION_SIZE_MIN;
                    populationSize <= POPULATION_SIZE_MAX;
                    populationSize += POPULATION_SIZE_STEP
            ) {
                double bestFitness = 0.0; // best fitness for current population size
                String bestChrom = "";    // best chromosome for current population size
                double accFitness = 0.0;  // accumulated fitness for current population size

                // repeat defined number of times
                for (int loopIdx = 0; loopIdx < LOOPS_PER_POPULATION_SIZE; loopIdx++) {
                    // run the algorithm
                    GAKnapsack gaKnapsack = new GAKnapsack(
                            inputFilename,
                            populationSize,
                            CROSSOVER_PROBABILITY,
                            RANDOM_SELECTION_CHANCE,
                            GENERATIONS,
                            MUTATION_PROBABILITY
                    );
                    gaKnapsack.run();

                    // compare results
                    String currChrom = ((ChromChars) gaKnapsack.getFittestChromosome()).getGenesAsStr();
                    double currChromFittness = gaKnapsack.getFittestChromosomesFitness();
                    if (currChromFittness > bestFitness) {
                        bestFitness = currChromFittness;
                        bestChrom = currChrom;
                    }
                    accFitness += currChromFittness;
                }

                // write results to a file
                writer.write(String.format("%d,", populationSize));
                writer.write(String.format("%s,", bestChrom));
                writer.write(String.format("%f,", bestFitness));
                writer.write(String.format("%f\n", accFitness / LOOPS_PER_POPULATION_SIZE));
            }

            writer.close();
        } catch (GAException ex) {
            System.out.printf("Error occurred on initialization: %s", ex.getMessage());
        } catch (FileNotFoundException ex) {
            System.out.printf("Input file not found: %s", ex.getMessage());
        } catch (IOException ex) {
            System.out.printf("Output file not found or could not be created: %s", ex.getMessage());
        }
    }

}
