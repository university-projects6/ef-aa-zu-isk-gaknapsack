package services;

import com.softtechdesign.ga.ChromChars;
import com.softtechdesign.ga.GAException;
import model.GAKnapsack;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Runs an experiment to define optimal population size for knapsack problem
 */
public class CrossoverProbabilityRunner {

    /**
     * Starting value of the crossover probability
     */
    private static final double CROSSOVER_PROBABILITY_MIN = 1.0;
    /**
     * Ending value of the crossover probability
     */
    private static final double CROSSOVER_PROBABILITY_MAX = 1.0;
    /**
     * Crossover probability loop step
     */
    private static final double CROSSOVER_PROBABILITY_STEP = 0.01;
    /**
     * Number of loops done on single crossover probability
     */
    private static final int LOOPS_PER_CROSSOVER_PROBABILITY = 10;

    /**
     * Size of the population
     */
    private static final int POPULATION_SIZE = 820;
    /**
     * Random selection chance
     */
    private static final int RANDOM_SELECTION_CHANCE = 5;
    /**
     * Number of generations
     */
    private static final int GENERATIONS = 1000;
    /**
     * Chromosome mutation probability
     */
    private static final double MUTATION_PROBABILITY = 0.01;

    /**
     * Run the experiment to determine best crossover probability
     * @param inputFilename name of the file with input data
     * @param outputFilename name of the file to write results to
     */
    public void runCrossoverProbabilityLoop(String inputFilename, String outputFilename) {
        try {
            // create writer to store algorithm results in a file
            String outputPath = "./src/output/";
            BufferedWriter writer = new BufferedWriter(
                    new FileWriter(outputPath.concat(outputFilename))
            );

            // write file headers
            writer.write("Crossover probability,Best chromosome,Best fitness,Avg fitness\n");

            // loop through crossover probability
            for (
                    double crossoverProbability = CROSSOVER_PROBABILITY_MIN;
                    crossoverProbability <= CROSSOVER_PROBABILITY_MAX;
                    crossoverProbability += CROSSOVER_PROBABILITY_STEP
            ) {
                double bestFitness = 0.0; // best fitness for current crossover probability
                String bestChrom = "";    // best chromosome for current crossover probability
                double accFitness = 0.0;  // accumulated fitness for current crossover probability

                // repeat defined number of times
                for (int loopIdx = 0; loopIdx < LOOPS_PER_CROSSOVER_PROBABILITY; loopIdx++) {
                    // run the algorithm
                    GAKnapsack gaKnapsack = new GAKnapsack(
                            inputFilename,
                            POPULATION_SIZE,
                            crossoverProbability,
                            RANDOM_SELECTION_CHANCE,
                            GENERATIONS,
                            MUTATION_PROBABILITY
                    );
                    gaKnapsack.run();

                    // compare results
                    String currChrom = ((ChromChars) gaKnapsack.getFittestChromosome()).getGenesAsStr();
                    double currChromFittness = gaKnapsack.getFittestChromosomesFitness();
                    if (currChromFittness > bestFitness) {
                        bestFitness = currChromFittness;
                        bestChrom = currChrom;
                    }
                    accFitness += currChromFittness;
                }

                // write results to a file
                writer.write(String.format("%f,", crossoverProbability));
                writer.write(String.format("%s,", bestChrom));
                writer.write(String.format("%f,", bestFitness));
                writer.write(String.format("%f\n", accFitness / LOOPS_PER_CROSSOVER_PROBABILITY));
            }

            writer.close();
        } catch (GAException ex) {
            System.out.printf("Error occurred on initialization: %s", ex.getMessage());
        } catch (FileNotFoundException ex) {
            System.out.printf("Input file not found: %s", ex.getMessage());
        } catch (IOException ex) {
            System.out.printf("Output file not found or could not be created: %s", ex.getMessage());
        }
    }

}
