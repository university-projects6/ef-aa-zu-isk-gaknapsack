package services;

import com.softtechdesign.ga.ChromChars;
import com.softtechdesign.ga.GAException;
import model.GAKnapsack;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Runs an experiment to define optimal random selection chance for knapsack problem
 */
public class RandomSelectionChanceRunner {

    /**
     * Starting value of the random selection chance
     */
    private static final int RANDOM_SELECTION_CHANCE_MIN = 1;
    /**
     * Ending value of the random selection chance
     */
    private static final int RANDOM_SELECTION_CHANCE_MAX = 100;
    /**
     * Random selection chance loop step
     */
    private static final int RANDOM_SELECTION_CHANCE_STEP = 1;
    /**
     * Number of loops done on single random selection chance
     */
    private static final int LOOPS_PER_RANDOM_SELECTION_CHANCE = 10;

    /**
     * Size of the population
     */
    private static final int POPULATION_SIZE = 820;
    /**
     * Crossover probability
     */
    private static final double CROSSOVER_PROBABILITY = 0.75;
    /**
     * Number of generations
     */
    private static final int GENERATIONS = 1000;
    /**
     * Chromosome mutation probability
     */
    private static final double MUTATION_PROBABILITY = 0.01;

    /**
     * Run the experiment to determine best random selection chance
     * @param inputFilename name of the file with input data
     * @param outputFilename name of the file to write results to
     */
    public void runRandomSelectionChanceLoop(String inputFilename, String outputFilename) {
        try {
            // create writer to store algorithm results in a file
            String outputPath = "./src/output/";
            BufferedWriter writer = new BufferedWriter(
                    new FileWriter(outputPath.concat(outputFilename))
            );

            // write file headers
            writer.write("Random selection chance,Best chromosome,Best fitness,Avg fitness\n");

            // loop through random selection chance
            for (
                    int randomSelectionChance = RANDOM_SELECTION_CHANCE_MIN;
                    randomSelectionChance <= RANDOM_SELECTION_CHANCE_MAX;
                    randomSelectionChance += RANDOM_SELECTION_CHANCE_STEP
            ) {
                double bestFitness = 0.0; // best fitness for current random selection chance
                String bestChrom = "";    // best chromosome for current random selection chance
                double accFitness = 0.0;  // accumulated fitness for current random selection chance

                // repeat defined number of times
                for (int loopIdx = 0; loopIdx < LOOPS_PER_RANDOM_SELECTION_CHANCE; loopIdx++) {
                    // run the algorithm
                    GAKnapsack gaKnapsack = new GAKnapsack(
                            inputFilename,
                            POPULATION_SIZE,
                            CROSSOVER_PROBABILITY,
                            randomSelectionChance,
                            GENERATIONS,
                            MUTATION_PROBABILITY
                    );
                    gaKnapsack.run();

                    // compare results
                    String currChrom = ((ChromChars) gaKnapsack.getFittestChromosome()).getGenesAsStr();
                    double currChromFittness = gaKnapsack.getFittestChromosomesFitness();
                    if (currChromFittness > bestFitness) {
                        bestFitness = currChromFittness;
                        bestChrom = currChrom;
                    }
                    accFitness += currChromFittness;
                }

                // write results to a file
                writer.write(String.format("%d,", randomSelectionChance));
                writer.write(String.format("%s,", bestChrom));
                writer.write(String.format("%f,", bestFitness));
                writer.write(String.format("%f\n", accFitness / LOOPS_PER_RANDOM_SELECTION_CHANCE));
            }

            writer.close();
        } catch (GAException ex) {
            System.out.printf("Error occurred on initialization: %s", ex.getMessage());
        } catch (FileNotFoundException ex) {
            System.out.printf("Input file not found: %s", ex.getMessage());
        } catch (IOException ex) {
            System.out.printf("Output file not found or could not be created: %s", ex.getMessage());
        }
    }

}
