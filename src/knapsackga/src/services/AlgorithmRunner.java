package services;

import com.softtechdesign.ga.ChromChars;
import com.softtechdesign.ga.GAException;
import model.GAKnapsack;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Runs genetic algorithm for selected arguments
 */
public class AlgorithmRunner {

    /**
     * Size of the population
     */
    private static final int POPULATION_SIZE = 820;
    /**
     * Crossover probability
     */
    private static final double CROSSOVER_PROBABILITY = 0.75;
    /**
     * Random selection chance
     */
    private static final int RANDOM_SELECTION_CHANCE = 88;
    /**
     * Number of generations
     */
    private static final int GENERATIONS = 7700;
    /**
     * Chromosome mutation probability
     */
    private static final double MUTATION_PROBABILITY = 0.05;

    /**
     * Repeat algorithm set number of times
     */
    private static final int LOOP_ALGORITHM = 10;

    /**
     * Run genetic algorithm with defined parameters
     * @param inputFilename name of the file with input data
     * @param outputFilename name of the file to write results to
     */
    public void runAlgorithm(String inputFilename, String outputFilename) {
        try {
            // create writer to store algorithm results in a file
            String outputPath = "./src/output/";
            BufferedWriter writer = new BufferedWriter(
                    new FileWriter(outputPath.concat(outputFilename))
            );

            // write file headers
            writer.write("Best chromosome,Best fitness\n");

            // run the algorithm set number of times
            for (
                    int loopIdx = 0;
                    loopIdx < LOOP_ALGORITHM;
                    loopIdx++
            ) {
                GAKnapsack gaKnapsack = new GAKnapsack(
                        inputFilename,
                        POPULATION_SIZE,
                        CROSSOVER_PROBABILITY,
                        RANDOM_SELECTION_CHANCE,
                        GENERATIONS,
                        MUTATION_PROBABILITY
                );
                gaKnapsack.run();

                // write results to a file
                String bestChrom = ((ChromChars) gaKnapsack.getFittestChromosome()).getGenesAsStr();
                writer.write(String.format("%s,", bestChrom));
                double chromFittness = gaKnapsack.getFittestChromosomesFitness();
                writer.write(String.format("%f\n", chromFittness));
            }

            writer.close();
        } catch (GAException ex) {
            System.out.printf("Error occurred on initialization: %s", ex.getMessage());
        } catch (FileNotFoundException ex) {
            System.out.printf("Input file not found: %s", ex.getMessage());
        } catch (IOException ex) {
            System.out.printf("Output file not found or could not be created: %s", ex.getMessage());
        }
    }

}
