package services;

import com.softtechdesign.ga.ChromChars;
import com.softtechdesign.ga.GAException;
import model.GAKnapsack;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Runs an experiment to define optimal number of generations for knapsack problem
 */
public class GenerationsRunner {

    /**
     * Starting value of the number of generations
     */
    private static final int GENERATIONS_MIN = 100;
    /**
     * Ending value of the number of generations
     */
    private static final int GENERATIONS_MAX = 10000;
    /**
     * Number of generations loop step
     */
    private static final int GENERATIONS_STEP = 100;
    /**
     * Number of loops done on single number of generations
     */
    private static final int LOOPS_PER_GENERATIONS = 10;

    /**
     * Size of the population
     */
    private static final int POPULATION_SIZE = 820;
    /**
     * Crossover probability
     */
    private static final double CROSSOVER_PROBABILITY = 0.75;
    /**
     * Random selection chance
     */
    private static final int RANDOM_SELECTION_CHANCE = 88;
    /**
     * Chromosome mutation probability
     */
    private static final double MUTATION_PROBABILITY = 0.05;

    /**
     * Run the experiment to determine best number of generations
     * @param inputFilename name of the file with input data
     * @param outputFilename name of the file to write results to
     */
    public void runGenerationsLoop(String inputFilename, String outputFilename) {
        try {
            // create writer to store algorithm results in a file
            String outputPath = "./src/output/";
            BufferedWriter writer = new BufferedWriter(
                    new FileWriter(outputPath.concat(outputFilename))
            );

            // write file headers
            writer.write("Generations,Best chromosome,Best fitness,Avg fitness\n");

            // loop through number of generations
            for (
                    int generations = GENERATIONS_MIN;
                    generations <= GENERATIONS_MAX;
                    generations += GENERATIONS_STEP
            ) {
                double bestFitness = 0.0; // best fitness for current number of generations
                String bestChrom = "";    // best chromosome for current number of generations
                double accFitness = 0.0;  // accumulated fitness for current number of generations

                // repeat defined number of times
                for (int loopIdx = 0; loopIdx < LOOPS_PER_GENERATIONS; loopIdx++) {
                    // run the algorithm
                    GAKnapsack gaKnapsack = new GAKnapsack(
                            inputFilename,
                            POPULATION_SIZE,
                            CROSSOVER_PROBABILITY,
                            RANDOM_SELECTION_CHANCE,
                            generations,
                            MUTATION_PROBABILITY
                    );
                    gaKnapsack.run();

                    // compare results
                    String currChrom = ((ChromChars) gaKnapsack.getFittestChromosome()).getGenesAsStr();
                    double currChromFittness = gaKnapsack.getFittestChromosomesFitness();
                    if (currChromFittness > bestFitness) {
                        bestFitness = currChromFittness;
                        bestChrom = currChrom;
                    }
                    accFitness += currChromFittness;
                }

                // write results to a file
                writer.write(String.format("%d,", generations));
                writer.write(String.format("%s,", bestChrom));
                writer.write(String.format("%f,", bestFitness));
                writer.write(String.format("%f\n", accFitness / LOOPS_PER_GENERATIONS));
            }

            writer.close();
        } catch (GAException ex) {
            System.out.printf("Error occurred on initialization: %s", ex.getMessage());
        } catch (FileNotFoundException ex) {
            System.out.printf("Input file not found: %s", ex.getMessage());
        } catch (IOException ex) {
            System.out.printf("Output file not found or could not be created: %s", ex.getMessage());
        }
    }

}
