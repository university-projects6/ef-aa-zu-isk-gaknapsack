package services;

import com.softtechdesign.ga.ChromChars;
import com.softtechdesign.ga.GAException;
import model.GAKnapsack;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Runs an experiment to define optimal mutation probability for knapsack problem
 */
public class MutationProbabilityRunner {

    /**
     * Starting value of the mutation probability
     */
    private static final double MUTATION_PROBABILITY_MIN = 0.01;
    /**
     * Ending value of the mutation probability
     */
    private static final double MUTATION_PROBABILITY_MAX = 1.0;
    /**
     * Mutation probability loop step
     */
    private static final double MUTATION_PROBABILITY_STEP = 0.01;
    /**
     * Number of loops done on single mutation probability
     */
    private static final int LOOPS_PER_MUTATION_PROBABILITY = 10;

    /**
     * Size of the population
     */
    private static final int POPULATION_SIZE = 820;
    /**
     * Crossover probability
     */
    private static final double CROSSOVER_PROBABILITY = 0.75;
    /**
     * Random selection chance
     */
    private static final int RANDOM_SELECTION_CHANCE = 88;
    /**
     * Number of generations
     */
    private static final int GENERATIONS = 1000;

    /**
     * Run the experiment to determine best mutation probability
     * @param inputFilename name of the file with input data
     * @param outputFilename name of the file to write results to
     */
    public void runMutationProbabilityLoop(String inputFilename, String outputFilename) {
        try {
            // create writer to store algorithm results in a file
            String outputPath = "./src/output/";
            BufferedWriter writer = new BufferedWriter(
                    new FileWriter(outputPath.concat(outputFilename))
            );

            // write file headers
            writer.write("Mutation probability,Best chromosome,Best fitness,Avg fitness\n");

            // loop through mutation probability
            for (
                    double mutationProbability = MUTATION_PROBABILITY_MIN;
                    mutationProbability <= MUTATION_PROBABILITY_MAX;
                    mutationProbability += MUTATION_PROBABILITY_STEP
            ) {
                double bestFitness = 0.0; // best fitness for current mutation probability
                String bestChrom = "";    // best chromosome for current mutation probability
                double accFitness = 0.0;  // accumulated fitness for current mutation probability

                // repeat defined number of times
                for (int loopIdx = 0; loopIdx < LOOPS_PER_MUTATION_PROBABILITY; loopIdx++) {
                    // run the algorithm
                    GAKnapsack gaKnapsack = new GAKnapsack(
                            inputFilename,
                            POPULATION_SIZE,
                            CROSSOVER_PROBABILITY,
                            RANDOM_SELECTION_CHANCE,
                            GENERATIONS,
                            mutationProbability
                    );
                    gaKnapsack.run();

                    // compare results
                    String currChrom = ((ChromChars) gaKnapsack.getFittestChromosome()).getGenesAsStr();
                    double currChromFittness = gaKnapsack.getFittestChromosomesFitness();
                    if (currChromFittness > bestFitness) {
                        bestFitness = currChromFittness;
                        bestChrom = currChrom;
                    }
                    accFitness += currChromFittness;
                }

                // write results to a file
                writer.write(String.format("%f,", mutationProbability));
                writer.write(String.format("%s,", bestChrom));
                writer.write(String.format("%f,", bestFitness));
                writer.write(String.format("%f\n", accFitness / LOOPS_PER_MUTATION_PROBABILITY));
            }

            writer.close();
        } catch (GAException ex) {
            System.out.printf("Error occurred on initialization: %s", ex.getMessage());
        } catch (FileNotFoundException ex) {
            System.out.printf("Input file not found: %s", ex.getMessage());
        } catch (IOException ex) {
            System.out.printf("Output file not found or could not be created: %s", ex.getMessage());
        }
    }

}
