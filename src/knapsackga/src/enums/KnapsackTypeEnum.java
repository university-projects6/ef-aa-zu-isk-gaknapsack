package enums;

/**
 * Types of knapsack problem
 */
public enum KnapsackTypeEnum {
    /**
     * Binary - gene values 0 or 1
     */
    BINARY (1),
    /**
     * Linear - gene values between 0 and 1
     */
    LINEAR (2);

    private final int value;

    KnapsackTypeEnum(int value) {
        this.value = value;
    }

    /**
     * Get gene values range for specific type
     * @return values range
     */
    public String getValuesRange() {
        switch (this.value) {
            case 1:
                return "01";
            case 2:
                return "0123456789";
            default:
                return "";
        }
    }

}
