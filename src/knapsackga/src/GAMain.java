import enums.RunTypeEnum;
import services.*;

public class GAMain {

    /**
     * Input file name
     */
    private static final String INPUT_FILENAME = "input.txt";
    /**
     * Output file name
     */
    private static final String OUTPUT_FILENAME = "linear_final_algorithm.csv";
    /**
     * Defines type of program run (algorithm or experiments)
     */
    private static final RunTypeEnum RUN_TYPE = RunTypeEnum.ALGORITHM;

    /**
     * Main method - runs the algorithm
     * @param args main function arguments
     */
    public static void main(String[] args) {
        System.out.println("Solving knapsack problem using genetic algorithm...");

        switch (RUN_TYPE) {
            case ALGORITHM:
                AlgorithmRunner algorithmRunner = new AlgorithmRunner();
                algorithmRunner.runAlgorithm(INPUT_FILENAME, OUTPUT_FILENAME);
                break;
            case POPULATION_SIZE_LOOP:
                PopulationSizeRunner populationSizeRunner = new PopulationSizeRunner();
                populationSizeRunner.runPopulationSizeLoop(INPUT_FILENAME, OUTPUT_FILENAME);
                break;
            case CROSSOVER_PROBABILITY_LOOP:
                CrossoverProbabilityRunner crossoverProbabilityRunner = new CrossoverProbabilityRunner();
                crossoverProbabilityRunner.runCrossoverProbabilityLoop(INPUT_FILENAME, OUTPUT_FILENAME);
                break;
            case RANDOM_SELECTION_CHANCE_LOOP:
                RandomSelectionChanceRunner randomSelectionChanceRunner = new RandomSelectionChanceRunner();
                randomSelectionChanceRunner.runRandomSelectionChanceLoop(INPUT_FILENAME, OUTPUT_FILENAME);
                break;
            case GENERATIONS_LOOP:
                GenerationsRunner generationsRunner = new GenerationsRunner();
                generationsRunner.runGenerationsLoop(INPUT_FILENAME, OUTPUT_FILENAME);
                break;
            case MUTATION_PROBABILITY_LOOP:
                MutationProbabilityRunner mutationProbabilityRunner = new MutationProbabilityRunner();
                mutationProbabilityRunner.runMutationProbabilityLoop(INPUT_FILENAME, OUTPUT_FILENAME);
                break;
            default:
                System.out.println("Incorrect run type");
        }
    }

}
